$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:2500
    });

    $('#notificar').on('show.bs.modal', function(e){
        console.log('Se comenzó a abrir el modal');
        $('#contactobtn').prop('disabled',true);
    });
    $('#notificar').on('shown.bs.modal', function(e){
        console.log('Se terminó de abrir el modal');
    });
    $('#notificar').on('hide.bs.modal', function(e){
        console.log('Se empezó a ocultar el modal');
    });
    $('#notificar').on('hidden.bs.modal', function(e){
        console.log('Se ocultó el modal');
        console.log('Se comenzó a abrir el modal');
        $('#contactobtn').prop('disabled',false);
    });
});